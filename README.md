# README #

Application that allows remote users to collaborate on a shared video instance and chat simultaneously. Similar to the functionality that Google Hangouts provides with its YouTube application. Currently set up to run locally through port 3700 - if you want to host this on a public network, you will need to forward the port and change the IP address in chat.js.

### Who do I talk to? ###

* adrian (ashatte@gmail.com)